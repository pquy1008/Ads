stdClass Object
(
    [request] => stdClass Object
        (
            [params] => stdClass Object
                (
                    [account_id] => ftr8ac
                )

        )

    [data] => Array
        (
            [0] => stdClass Object
                (
                    [targetable] => 
                    [name] => testCK
                    [targetable_types] => Array
                        (
                            [0] => CRM
                            [1] => EXCLUDED_CRM
                        )

                    [audience_type] => CRM
                    [id] => lnmj
                    [reasons_not_targetable] => Array
                        (
                            [0] => TOO_SMALL
                        )

                    [list_type] => TWITTER_ID
                    [created_at] => 2014-10-06T10:05:56Z
                    [updated_at] => 2014-10-06T16:09:16Z
                    [partner_source] => OTHER
                    [deleted] => 
                    [audience_size] => 
                )

            [1] => stdClass Object
                (
                    [targetable] => 
                    [name] => test
                    [targetable_types] => Array
                        (
                            [0] => WEB
                            [1] => EXCLUDED_WEB
                        )

                    [audience_type] => WEB
                    [id] => lo8v
                    [reasons_not_targetable] => Array
                        (
                            [0] => TOO_SMALL
                        )

                    [list_type] => 
                    [created_at] => 2014-10-07T10:52:05Z
                    [updated_at] => 2014-10-07T10:52:05Z
                    [partner_source] => OTHER
                    [deleted] => 
                    [audience_size] => 
                )

            [2] => stdClass Object
                (
                    [targetable] => 
                    [name] => サイト訪問者A
                    [targetable_types] => Array
                        (
                            [0] => WEB
                            [1] => EXCLUDED_WEB
                        )

                    [audience_type] => WEB
                    [id] => qgq5
                    [reasons_not_targetable] => Array
                        (
                            [0] => TOO_SMALL
                        )

                    [list_type] => 
                    [created_at] => 2015-01-27T05:16:24Z
                    [updated_at] => 2015-01-27T05:16:24Z
                    [partner_source] => OTHER
                    [deleted] => 
                    [audience_size] => 
                )

            [3] => stdClass Object
                (
                    [targetable] => 
                    [name] => APキャンペーン
                    [targetable_types] => Array
                        (
                            [0] => WEB
                            [1] => EXCLUDED_WEB
                        )

                    [audience_type] => WEB
                    [id] => r5tm
                    [reasons_not_targetable] => Array
                        (
                            [0] => TOO_SMALL
                        )

                    [list_type] => 
                    [created_at] => 2015-03-10T06:20:52Z
                    [updated_at] => 2015-03-10T06:20:52Z
                    [partner_source] => OTHER
                    [deleted] => 
                    [audience_size] => 
                )

        )

    [data_type] => tailored_audiences
    [total_count] => 4
    [next_cursor] => 
)
